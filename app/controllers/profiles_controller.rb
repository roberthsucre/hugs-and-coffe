class ProfilesController < ApplicationController
  before_action :authenticate_user!, only: [:edit, :update]
  before_action :set_profile, only: [:edit, :update, :show]

  before_action :check_current_user, only: [:edit, :update]

  before_action :sort_me, only: [:update]

  def edit
  end

  def update
    if @profile.update(profile_params)
      @profile.update({
        m1: @multipliers[0],
        m2: @multipliers[1],
        m3: @multipliers[2],
        m4: @multipliers[3]
      })
      redirect_to edit_profile_path(@profile.id), notice: 'YES EDITED'
    else
      render :edit
    end
  end

  def show
  end

  private

  def sort_me
    @multipliers = []
    @multipliers << params[:profile][:m1].to_i
    @multipliers << params[:profile][:m2].to_i
    @multipliers << params[:profile][:m3].to_i
    @multipliers << params[:profile][:m4].to_i
    @multipliers.sort!
  end

  def set_profile
    @profile = Profile.find(params[:id])
  end

  def check_current_user
    if current_user.id != @profile.id
      redirect_to root_path
    end
  end

  def profile_params
    params.require(:profile).permit(:fullname, :bio, :occupation, :amount, :currency, :m1, :m2, :m3, :m4, :avatar)
  end
end
