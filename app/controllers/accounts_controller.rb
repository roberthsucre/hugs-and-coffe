class AccountsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_profile, only: [:index, :new, :create]
  before_action :set_account, only: [:edit, :update, :destroy]

  before_action :check_current_user, only: [:index, :new]

  def index
    @accounts = @profile.accounts
  end

  def new
    @account = Account.new
  end

  def create
    @account = @profile.accounts.build(account_params)
    if @account.save
      redirect_to profile_accounts_path(@profile), notice: 'Account was successfully created.'
    else
      render :new
    end
  end

  def edit
  end

  def update
    @profile = @account.profile.id
    if @account.update(account_params)
      redirect_to profile_accounts_path(@profile), notice: 'Your info was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @profile = @account.profile.id
    @account.destroy
    redirect_to profile_accounts_path(@profile), notice: 'Info was successfully destroyed.'
  end

  private

  def set_profile
    @profile = Profile.find(params[:profile_id])
  end

  def set_account
    @account = Account.find(params[:id])
  end

  def check_current_user
    if current_user.id != @profile.id
      redirect_to root_path
    end
  end

  def account_params
    params.require(:account).permit(:bank, :kind, :account_number, :email, :cedula)
  end
end
