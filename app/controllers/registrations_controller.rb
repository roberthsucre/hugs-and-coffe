class RegistrationsController < Devise::RegistrationsController
  after_action :create_profile

  private

 def after_sign_up_path_for(resource)
   #but no! can't be the user id.....has to be the profile id
   "/profiles/#{ resource.id }/edit"
 end

 def create_profile
   if resource.persisted?
     @profile = resource.build_profile({
       fullname: "",
       occupation: "",
       bio: "",
       amount: "",
       m1: 1,
       m2: 2,
       m3: 3,
       m4: 5,
       currency: ""
     });
     @profile.save
   end
 end

end
