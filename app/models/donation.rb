class Donation < ApplicationRecord
  belongs_to :donable, polymorphic: true
end
