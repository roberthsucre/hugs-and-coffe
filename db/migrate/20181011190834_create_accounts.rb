class CreateAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :accounts do |t|
      t.references :profile, foreign_key: true
      t.string :bank
      t.string :kind
      t.string :account_number
      t.string :email
      t.string :cedula


      t.timestamps
    end
  end
end
