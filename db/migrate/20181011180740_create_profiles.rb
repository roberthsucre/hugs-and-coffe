class CreateProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :profiles do |t|
      t.references :user, foreign_key: true
      t.string :fullname
      t.string :occupation
      t.text :bio
      t.decimal :amount
      t.string :currency
      t.integer :m1
      t.integer :m2
      t.integer :m3
      t.integer :m4

      t.timestamps
    end
  end
end
