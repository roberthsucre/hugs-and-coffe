Rails.application.routes.draw do

  get 'donationsmanager/menu'
  get 'donationsmanager/thanks'
  devise_for :users, controllers: { registrations: 'registrations'}
  root to: 'app#home'

  resources :profiles, only: [:show, :edit, :update], shallow: true do
    resources :images, except: :show
    resources :accounts, except: :show
    resources :beverages, only: [:new, :create]
  end

end
